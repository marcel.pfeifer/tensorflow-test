async function getData() {
    const carsDataResponse = await fetch('https://storage.googleapis.com/tfjs-tutorials/carsData.json')
    const carsData = await carsDataResponse.json()
    return carsData.map(car => {
        return {
            mpg: car.Miles_per_Gallon,
            horsepower: car.Horsepower
        }
    }).filter(car => car.mpg !== null && car.horsepower !== null)
}

function createModel() {
    const model = tf.sequential()

    model.add(tf.layers.dense({
        inputShape: [1],
        units: 1,
        useBias: true,
    }))

    model.add(tf.layers.dense({units: 50, activation: 'relu'}));

    model.add(tf.layers.dense({units: 20, activation: 'relu'}));

    model.add(tf.layers.dense({units: 20, activation: 'relu'}));

    model.add(tf.layers.dense({units: 50, activation: 'sigmoid'}));

    model.add(tf.layers.dense({
        units: 1,
        useBias: true
    }))

    return model
}

function convertToTensor(data) {
    return tf.tidy(() => {
        tf.util.shuffle(data)

        const inputs = data.map(point => point.horsepower)
        const labels = data.map(point => point.mpg)

        const inputTensor = tf.tensor2d(inputs, [inputs.length, 1])
        const labelTensor = tf.tensor2d(labels, [labels.length, 1])

        const inputMax = inputTensor.max()
        const inputMin = inputTensor.min()
        const labelMax = labelTensor.max()
        const labelMin = labelTensor.min()

        const normalizeInputs = inputTensor.sub(inputMin).div(inputMax.sub(inputMin))
        const normalizeLabels = labelTensor.sub(labelMin).div(labelMax.sub(labelMin))

        return {
            inputs: normalizeInputs,
            labels: normalizeLabels,
            labelMax,
            labelMin,
            inputMax,
            inputMin
        }
    })
}

async function trainModel(model, inputs, labels) {
    model.compile({
        optimizer: tf.train.adam(),
        loss: tf.losses.meanSquaredError,
        metrics: ['mse']
    })

    const batchSize = 32
    const epochs = 100

    return await model.fit(inputs, labels, {
        batchSize,
        epochs,
        shuffle: true, 
        callbacks: tfvis.show.fitCallbacks(
            {
                name: 'Trainings Performance'
            },
            ['loss', 'mse'],
            {
                height: 200,
                callbacks: ['onEpochEnd']
            }
           
        )
    })
}

function testModel(model, inputData, normalizationData) {
    const {inputMax, inputMin, labelMax, labelMin} = normalizationData
    const [xs, predictions] = tf.tidy(() => {
        const xs = tf.linspace(0, 1, 100)

        const xsReshaped = xs.reshape([100, 1])

        const predictions = model.predict(xsReshaped)

        const unnormalizeXs = xs.mul(inputMax.sub(inputMin)).add(inputMin)
        const unnormalizePredictions = predictions.mul(labelMax.sub(labelMin)).add(labelMin)

        return [unnormalizeXs.dataSync(), unnormalizePredictions.dataSync()]
    })

    const predictedPoints = Array.from(xs).map((val, i) => ({
        x: val,
        y: predictions[i]
    }))

    const originalPoints = inputData.map(point => ({
        x: point.horsepower,
        y: point.mpg
    }))

    tfvis.render.scatterplot(
        {
            name: 'Model Predictions vs Data'
        },
        {
            values: [originalPoints, predictedPoints],
            series: ['original', 'prediction']
        },
        {
            xLabel: 'Horsepower',
            yLabel: 'MPG',
            height: 300
        }
    )
}

async function run() {
    const data = await getData()
    const values = data.map(point => {
        return {
            x: point.horsepower,
            y: point.mpg
        }
    })

    tfvis.render.scatterplot({
        name: 'Horsepower v MPG'
    }, {
        values
    }, {
        xLabel: 'Horsepower',
        yLabel: 'MPG',
        height: 300
    })

    const model = createModel()

    tfvis.show.modelSummary(
        {
            name: 'Model Summary'
        },
        model
    )

    const tensorData = convertToTensor(data)
    const {inputs, labels} = tensorData
    await trainModel(model, inputs, labels)
    console.log('Done Training');

    testModel(model, data, tensorData)
}

document.addEventListener('DOMContentLoaded', run)